import 'package:aas_app/menu.dart';
import 'package:flutter/material.dart';
import 'masuk.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';
import 'package:firebase_auth/firebase_auth.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: MaterialColor(0xFFA07ECA, {
          50: Color(0xFFF3E5F1),
          100: Color(0xFFE0BFD8),
          200: Color(0xFFCB9DBE),
          300: Color(0xFFB37BA5),
          400: Color(0xFFA06292),
          500: Color(0xFF875B79),
          600: Color(0xFF74546B),
          700: Color(0xFF5C4C5A),
          800: Color(0xFF4A4350),
          900: Color(0xFF3B3A44),
        }),
      ),
      home: StreamBuilder<User?>(
        stream: FirebaseAuth.instance.userChanges(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Menu();
          } else {
            return Masuk();
          }
        },
      ),
    );
  }
}
