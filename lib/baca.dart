import 'package:aas_app/novel.dart';
import 'package:flutter/material.dart';

class Baca extends StatefulWidget {
  const Baca({super.key});
  @override
  AppState createState() => AppState();
}

class AppState extends State<Baca> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: 160,
                height: 240,
                child: Image.asset(
                  'assets/images/novel1.jpg',
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(height: 34.33),
              Text(
                " Not here to be liked",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontFamily: "Inter",
                  fontWeight: FontWeight.w600,
                ),
              ),
              SizedBox(height: 34.33),
              Container(
                width: 130,
                height: 35,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  color: Colors.black,
                ),
                padding: const EdgeInsets.only(
                  top: 9,
                  bottom: 8,
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      "Baca",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 15,
                        fontFamily: "Inter",
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 34.33),
              SizedBox(
                width: 308,
                child: Text(
                  "Phileas Fogg, yang bertaruh dengan temannya bahwa ia dapat mengelilingi dunia dalam waktu 80 hari. Dalam perjalanan, ia ditemani oleh pelayan cantiknya, Passepartout. Mereka menghadapi berbagai rintangan, seperti bencana alam, pencurian, dan kejar-kejaran  dengan polisi.",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 13,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
