import 'package:aas_app/bacaan.dart';
import 'package:aas_app/edit.dart';
import 'package:flutter/material.dart';

class ProfilPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Profil'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CircleAvatar(
            radius: 60,
            backgroundImage: AssetImage('assets/images/profil.png'),
          ),
          SizedBox(height: 16),
          Text(
            'Na Jaemin',
            style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 16),
          ElevatedButton(
            onPressed: () {
              // Tambahkan logika untuk aksi tombol edit profil di sini
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => EditProfilePage()),
              );
            },
            child: Text('Edit Profil'),
          ),
          SizedBox(height: 16),
          Expanded(
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                BacaanCard(judul: 'Bacaan 1'),
                BacaanCard(judul: 'Bacaan 2'),
                BacaanCard(judul: 'Bacaan 3'),
                BacaanCard(judul: 'Bacaan 4'),
                BacaanCard(judul: 'Bacaan 5'),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
