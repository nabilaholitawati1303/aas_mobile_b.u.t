import 'package:flutter/material.dart';

class BacaanCard extends StatelessWidget {
  final String judul;

  BacaanCard({required this.judul});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        title: Text(judul),
        trailing: IconButton(
          icon: Icon(Icons.delete),
          onPressed: () {
            // Tambahkan logika untuk menghapus bacaan di sini
          },
        ),
      ),
    );
  }
}
