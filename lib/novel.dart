class Novel {
  final String image;
  final String title;

  Novel({required this.image, required this.title});
}

List<Novel> novels = [
  Novel(
    image: 'assets/images/novel1.jpg',
    title: 'Novel 1',
  ),
  Novel(
    image: 'assets/images/novel2.jpg',
    title: 'Novel 2',
  ),
  Novel(
    image: 'assets/images/novel3.jpg',
    title: 'Novel 3',
  ),
];
