import 'package:aas_app/novel.dart';
import 'package:flutter/material.dart';

class BacaPage extends StatelessWidget {
  final Novel novel;

  const BacaPage({required this.novel});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(novel.title),
      ),
      body: Center(
        child: Text('Halaman Baca Novel: ${novel.title}'),
      ),
    );
  }
}
