import 'package:aas_app/masuk.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'menu.dart';

class Daftar extends StatefulWidget {
  const Daftar();
  @override
  AppState createState() => AppState();
}

class AppState extends State<Daftar> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController passwordValController = TextEditingController();

  bool errorValidator = false;

  Future<void> signUp() async {
    try {
      await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: emailController.text,
        password: passwordController.text,
      );
      await FirebaseAuth.instance.signOut();
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        print('The account already exists for that email.');
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: Column(
            children: [
              SizedBox(height: 90),
              Text(
                "Daftar",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
              SizedBox(height: 90),
              Container(
                width: 300,
                height: 55,
                child: TextField(
                  controller: emailController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(50),
                      borderSide: BorderSide(color: Colors.black, width: 1),
                    ),
                    filled: true,
                    fillColor: Colors.white,
                    hintText: "Email",
                  ),
                ),
              ),
              SizedBox(height: 30),
              Container(
                width: 300,
                height: 55,
                child: TextField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(50),
                        borderSide: BorderSide(color: Colors.black, width: 1),
                      ),
                      filled: true,
                      fillColor: Colors.white,
                      hintText: "Nama Pengguna"),
                ),
              ),
              SizedBox(height: 30),
              Container(
                width: 300,
                height: 55,
                child: TextField(
                  controller: passwordController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(50),
                      borderSide: BorderSide(color: Colors.black, width: 1),
                    ),
                    filled: true,
                    fillColor: Colors.white,
                    hintText: "Email",
                    errorText: errorValidator ? "Kata Sandi Salah!" : null,
                  ),
                  obscureText: true,
                ),
              ),
              SizedBox(height: 30),
              Container(
                width: 300,
                height: 55,
                child: TextField(
                  controller: passwordValController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(50),
                      borderSide: BorderSide(color: Colors.black, width: 1),
                    ),
                    filled: true,
                    fillColor: Colors.white,
                    hintText: "Konfirmasi Kata Sandi",
                    errorText: errorValidator ? "Kata Sandi Salah!" : null,
                  ),
                  obscureText: true,
                ),
              ),
              SizedBox(height: 55),
              Container(
                width: 300,
                height: 55,
                child: ElevatedButton(
                  onPressed: () {
                    setState(() {
                      passwordController.text == passwordValController.text
                          ? errorValidator = false
                          : errorValidator = true;
                    });
                    if (errorValidator) {
                      print("Error");
                    } else {
                      signUp();
                      Navigator.pop(context);
                    }
                  },
                  child: TextField(
                    enabled: false,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(50),
                        borderSide: BorderSide(color: Colors.white, width: 1),
                      ),
                      filled: true,
                      fillColor: Color(0xa5a07ecb),
                      hintText: "Daftar",
                      hintStyle: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontFamily: "Inter",
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
